# COMP5521-Group17

Blockchain Simulation Project Based on Naivecoins

## Getting started

Our project is based on :

https://github.com/conradoqg/naivecoin

Our Project UI can be accessed via the following public URLs :

http://35.161.83.70:3001/

http://35.161.83.70:3002/

http://35.161.83.70:3003/



If you like to test our Project in your local devices , type the following commands :

- cd naivecoin-master ;  and then

- npm install ; and then

- node bin/naivecoin.js --host 0.0.0.0 -p 3001 --name 1

- node bin/naivecoin.js --host 0.0.0.0 -p 3002 --name 2 --peers http://0.0.0.0:3001

- node bin/naivecoin.js --host 0.0.0.0 -p 3003 --name 3 --peers http://0.0.0.0:3001

You can test our UI locally :
http://0.0.0.0:3001/
http://0.0.0.0:3002/
http://0.0.0.0:3003/
