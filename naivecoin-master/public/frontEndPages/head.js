function printPageHeader(){

	listPages = [
		['displayBlocks.html', 'Display Blocks'],
		['createTx.html', 'Prepare / Create Transactions'],
		['listUnspent.html', 'List Unspent Transaction'],
		['mine.html', 'Mine Block'],
		['unconfirmedTx.html', 'Unconfirmed Transactions']
	];
	head = "<div>";
	for (let j = 0; j < listPages.length; j++){
		head += "<a href='" + listPages[j][0] + "'>" + listPages[j][1] + "</a>&emsp;";
	}
	head += "</div>";
	let url = window.location.pathname;
	let filename = url.substring(url.lastIndexOf('/')+1);
	//console.log(url.substring(url.lastIndexOf('/')+1));
	for (let j = 0; j < listPages.length; j++){
		if(listPages[j][0] == filename){
			head += "<h3>" + listPages[j][1] + "</h3>";
		}
	}
	
	head += "<br><table><tr><td>&emsp;Difficulty&emsp;</td><td><select id='clusterSel' onchange='clustering()'>";
		head += "<option value='3001'>1 - EVERY_X_BLOCKS = 5; POW_CURVE = 5</option>";
		head += "<option value='3011'>2 - EVERY_X_BLOCKS = 3; POW_CURVE = 5</option>";
		head += "<option value='3021'>3 - EVERY_X_BLOCKS = 5; POW_CURVE = 0</option>";
	head += "</select></td></tr></table>";


	document.getElementById("jsHeader").innerHTML = head;
	clustering();
}

function clustering(){
	cluster = document.getElementById("clusterSel").value;
	role = cluster;
	loadJson(role, walIdx);
}